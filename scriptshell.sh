#!\bin\bash
$a=1
$2=b
$(($a+$b))

#!\bin\bash
do {secs=$((1 * 60)) }
while [ $secs -gt 0 ]; do{
   echo -ne "$secs\033[0K\r"
   sleep 1 }
   : $((secs--))
done 

#!\bin\bash
do{ read-p nb=4
declare -a participants
i=0}
while [i -lt nb]
do{ read-p ("participant") pt
participants [$i]=$pt
i++}
done
